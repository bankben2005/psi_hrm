<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adminmenus extends Model
{
    protected $table = 'admin_menus';
    public $timestamps = true;
}