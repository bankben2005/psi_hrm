<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adminusers extends Model
{
    protected $table = 'admin_users';
    public $timestamps = true;
}