<?php
namespace App\Models;

use App\AdminUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Employee extends Model
{
    protected $table = 'employee';

    public function scopeActive($query)
    {
        return $query->where('employee.resignation_date',NULL);
    }

    public function adminUser(): HasMany
    {
        return $this->hasMany(AdminUser::class, 'employee_id', 'id');
    }
}