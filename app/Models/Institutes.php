<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Institutes extends Model
{
    protected $table = 'institutes';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}