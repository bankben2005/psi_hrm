<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warning extends Model
{
    protected $table = 'warning';
    public $timestamps = true;

    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}