<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Shiftmanagement extends Model
{
    protected $table = 'shift_management';

    public function shift(): BelongsTo
    {
        return $this->belongsTo(Shift::class);
    }
}