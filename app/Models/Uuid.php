<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Uuid extends Model
{
    protected $table = 'uuid';
    public $timestamps = true;
}