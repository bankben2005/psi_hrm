<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newstraffic extends Model
{
    protected $table = 'news_traffic';
    public $timestamps = true;
}