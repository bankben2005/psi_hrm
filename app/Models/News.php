<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    public $timestamps = true;
    public function scopeActive($query)
    {
        return $query->where('status', 'T');
    }
}