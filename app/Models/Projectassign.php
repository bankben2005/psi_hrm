<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
class Projectassign extends Model
{
    // use SoftDeletes;
    protected $table = 'project_assign';
    public $timestamps = true;
}