<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Corporateinformation extends Model
{
    protected $table = 'corporateinformation';
    public $timestamps = true;
}