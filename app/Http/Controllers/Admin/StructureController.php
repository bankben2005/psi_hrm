<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Structurepivot;
use App\Models\Structureevaluation;

use stdClass;

class StructureController extends Controller
{
    public function index()
    {
        $data['menu'] = 'โครงสร้างองค์กร';
        $data['employee'] = Employee::whereNull('employee.resignation_date')
        ->leftjoin(\DB::raw('groups'), 'groups.id', 'employee.group_id')
        ->leftjoin(\DB::raw('level'), 'level.id', 'employee.level_id')
        ->select([
            'employee.id as employee_id'
            ,\DB::raw('employee.firstname+\' \'+employee.lastname as name')
            ,\DB::raw('level.name as job_name')
            ,\DB::raw('level.job_description as job_description')
            ,\DB::raw('groups.name as group_name')
        ])
        ->get();
        return view('admin.orgranizational_structure', $data); // admin/orgranizational_structure
    }

    public function get_list(Request $request)
    {
        if (!empty($request->employee_id)) {
            $result = Employee::whereNull('employee.resignation_date')
            ->leftjoin(\DB::raw('groups'), 'groups.id', 'employee.group_id')
            ->leftjoin(\DB::raw('level'), 'level.id', 'employee.level_id')
            ->select([
                'employee.id as employee_id'
                ,\DB::raw('employee.firstname+\' \'+employee.lastname as name')
                ,\DB::raw('level.name as job_name')
                ,\DB::raw('level.job_description as job_description')
                ,\DB::raw('groups.name as group_name')
            ]);
            $result->whereIn('employee.id', $request->employee_id);
            return $data['employee'] = $result->get();
        } else {
            return $data['employee'] = [];
        }
    }

    public function post_organize(Request $request)
    {
        Structurepivot::truncate();
        $item = $request->all();
        $data = json_decode($item['structure'], true);
        return $this->children($data, null);
    }

    public function children($data, $top=null)
    {
        $s = array();
        foreach ($data as $key=>$value) {
            $obj = new stdClass();
            $box_id="";
            if ($top!=null) {
                $box_id = (string)$top.'_'.(string)($value['employee_id']);
            } else {
                $box_id = (string)($value['employee_id']);
            }

            $obj->box_top_id = (string)$top;
            $obj->box_id = $box_id;
            $obj->primary = $value['primary'];
            $obj->employee_id = $value['employee_id'];

            $insert = array();
            $insert['box_top_id'] = (string)$top;
            $insert['box_id'] = $box_id;
            $insert['employee_id'] = $value['employee_id'];
            $insert['created_at'] = date('Y-m-d H:i:s');
            $insert['primary'] = $value['primary'];
            $insert['sort'] = $key;
            if (!empty($value['children'])) {
                $obj->children = $this->children($value['children'], $obj->box_id);
            }
            $s[] = $obj;
            $result[] = Structurepivot::insert($insert);
        }
        return $s;
    }

    public function get_organize()
    {
        $result = Structurepivot::select([
            'structure_pivot.box_id'
            ,'structure_pivot.box_top_id'
            ,'employee.firstname'
            ,'employee.lastname'
            ,'employee.id as employee_id'
            ,'level.name as jobname'
            ,'structure_pivot.primary'
        ])
        ->leftjoin('employee', 'employee.id', 'structure_pivot.employee_id')
        ->leftjoin(\DB::raw('level'), 'level.id', 'employee.level_id')
        ->orderBy('structure_pivot.created_at', 'ASC')
        ->orderBy('structure_pivot.primary', 'DESC')
        ->orderBy('structure_pivot.sort', 'ASC')
        ->get();

        $return = [];
        foreach ($result as $key => $value) {
            if ($value->box_top_id==''||$value->box_top_id==null) {
                $obj = new stdClass();
                // $return = array();
                $obj->box_id = $value->box_id;
                $obj->primary = $value->primary;
                $obj->employee_id = $value->employee_id;
                $obj->employee_name = $value->firstname." ".$value->lastname;
                $obj->job_name = $value->jobname;
                unset($result[$key]);
                $obj->children = $this->get_organize_children($value->box_id, $result);
                $return[] = $obj;
            }
        }
        return $return;
    }

    public function get_organize_children($box_top_id, $result)
    {
        $return = array();
        foreach ($result as $key => $value) {
            if (strcasecmp((string)$value->box_top_id, (string)$box_top_id)==0) {
                $obj = new stdClass();
                $obj->primary = $value->primary;
                $obj->employee_id = $value->employee_id;
                $obj->employee_name = $value->firstname." ".$value->lastname;
                $obj->job_name = $value->jobname;
                $return[] = $obj;
                unset($result[$key]);
                $check_children = 0;
                foreach ($result as $k => $v) {
                    if (strcasecmp((string)$value->box_id, (string)$v->box_top_id)==0) {
                        $check_children++;
                        if ($check_children>0) {
                            $obj->children = $this->get_organize_children($v->box_top_id, $result);
                            break;
                        }
                    }
                }
            }
        }
        return $return;
    }

    public function orgranizational_chart()
    {
        $data['menu'] = 'แผนผังโครงสร้างองค์กร';
        $data['status'] = true;
        return view('admin.orgranizational_chart', $data);
    }

    public function orgranizational_chart_json($evaluation_id=null)
    {
        if (isset($evaluation_id)) {
            $table_name = 'structure_evaluation';
            $query = Structureevaluation::query();
            $query->where('evaluation_id', $evaluation_id);
        } else {
            $table_name = 'structure_pivot';
            $query = Structurepivot::query();
        }

        $query->select([
            "$table_name.box_id"
            ,"$table_name.*"
            ,"employee.*"
            ,"level.name as jobname"
        ]);
        $query->leftjoin("employee", "employee.id", "$table_name.employee_id");
        $query->leftjoin(\DB::raw("level"), "level.id", "employee.level_id");
        $query->orderBy("$table_name.created_at", "ASC");
        $query->orderBy("$table_name.primary", "DESC");
        $query->orderBy("$table_name.sort", "ASC");
        $result = $query->get();
        $return = array();
        foreach ($result as $key => $value) {
            $obj = new stdClass();
            $obj->id = $value->box_id;
            $obj->parent = $value->box_top_id;
            $obj->title = $value->firstname." ".$value->lastname;
            $obj->evaluation_id = $value->evaluation_id;
            $obj->description = $value->jobname;
            $obj->image = url($value->picture_profile);
            $obj->isVisible = true;
            $sub_obj = new stdClass();
            $sub_obj->width = 350;
            $sub_obj->height = 18;
            $obj->labelSize = $sub_obj;
            $return[] = $obj;
        }
        return $return;
    }

    public function structure_evaluation($evaluation_id=null)
    {
        $return = array();
        $insert = [];
        try {
            if (isset($evaluation_id)) {
                Structureevaluation::where('evaluation_id', $evaluation_id)->delete();
            }
            $result = Structurepivot::select('structure_pivot.*')->get();
            foreach ($result as $key => $value) {
                $item = [];
                $item['box_id'] = $value->box_id;
                $item['box_top_id'] = $value->box_top_id;
                $item['employee_id'] = $value->employee_id;
                $item['primary'] = $value->primary;
                $item['sort'] = $value->sort;
                $item['evaluation_id'] = $evaluation_id;
                $item['created_at'] = date('Y-m-d H:i:s');
                // $insert[] = $item;
                Structureevaluation::insert($item);
            }
            
            $return['status'] = true;
            $return['result_code'] = '000';
            $return['result_desc'] = 'success';
        } catch (\Exception $e) {
            $return['status'] = false;
            $return['result_code'] = '003';
            $return['result_desc'] = 'InsertDataException';
            $return['Exception'] = $e->getMessage();
        }
        return $return;
    }
}
