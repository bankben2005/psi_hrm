<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Shift;
use App\Models\Shiftmanagement;
use App\Models\Employee;


class ShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 'กะทำงาน';
        return view('admin.shift')->with($data);
    }

    public function manage_index()
    {
        $data['menu'] = 'ตารางกะทำงาน';
        $data['employee'] = Employee::get();
        $data['shift'] = Shift::get();
        return view('admin.manage_shift')->with($data);
    }

    public function management_list(Request $request)
    {
        $month = isset($request['month'])?$request['month']:date('Y-m');
        $date=date_create("$month");
        $days = (int)date_format($date,"t");
        $result = [];
        if($month){
            $query = Shiftmanagement::query();
            $query->select([
                'shift_management.employee_id'
                ,\DB::raw('employee.firstname +\' \'+ employee.lastname as employee_name')
            ]);
            $query->where('shift_management.date','like',"$month%");
            $query->leftjoin('employee','employee.id','shift_management.employee_id');
            $query->groupBy([
                'shift_management.employee_id'
                ,\DB::raw('employee.firstname + \' \' + employee.lastname')
            ]);
            $employee = $query->get();
    
            foreach ($employee as $key => $value) {
                $result[$key]['name'] = $value->employee_name;
                //start query get record
                $query_detail = Shiftmanagement::query();
                $query_detail->leftjoin('shift','shift.id','shift_management.shift_id');
                $query_detail->select([
                    'shift_management.id'
                    ,'shift_management.shift_id'
                    ,'shift_management.date'
                    ,'shift.name as shift_name'
                ]);
                $query_detail->where('shift_management.date','like',"$month%");
                if(isset($value->employee_id)){
                    $query_detail->where('shift_management.employee_id','=',$value->employee_id);
                }
                $return = $query_detail->get(); 
                //end query get record
    
                //start sort data
                $return_result = [];
                foreach ($return as $k => $value){
                    $d = $value->date;
                    $in_date = date_create("$d");
                    $in_day = (int)date_format($in_date,"d");
                    $value->shift_id = (int)$value->shift_id;
                    $return_result[$in_day][] = $value;
                }
                $return_result;
                $return_days = [];
                for ($i=1; $i<=$days ; $i++) {
                    $return_days[$i] = isset($return_result[$i]) ? $return_result[$i] : [];
                }
                $result[$key]['list'] = $return_days;
                //end sort data
            }
        }

        $data['month'] = $month;
        $data['days'] = $days;
        $data['data'] = $result;
        return response()->json($data);
    }

    public function list(){
        $model = Shift::query();
        $model->select([
            'shift.*'
            ,'shift.id as shiftid'
        ]);
        return  \DataTables::eloquent($model)
        ->addColumn('action',function($rec){
            $str = '
                <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->shiftid.'">
                    <i class="fa fa-edit"></i>
                </a>
                <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->shiftid.'">
                    <i class="fa fa-trash"></i>
                </a>
            ';
            return $str;
        })
        ->addIndexColumn()
        ->rawColumns(['action'])
        ->toJson();
    }

    public function get_shift_management(){
        return 1;
    }

    public function post_shift_management(Request $request){
        if(isset($request->date_start)&&isset($request->date_end)&&isset($request->employee_id)&&isset($request->shift_id)){
            $date_start=date_create($request->date_start);
            $date_end=date_create($request->date_end);
            $date_diff=date_diff($date_start,$date_end);
            $days = ($date_diff->d + 1);
            $shift_management = [];
            for ($i=0; $i < $days ; $i++) { 
                $insert = array();
                $insert['date'] = date("Y-m-d", strtotime("+$i day",strtotime($request->date_start)));
                $insert['employee_id'] = $request->employee_id;
                $insert['shift_id'] = $request->shift_id;
                $insert['created_at'] = date('Y-m-d');
                $shift_management[] = $insert;
            }
            \DB::beginTransaction();
            try{
                Shiftmanagement::insert($shift_management);
                \DB::commit();
                $data['status'] = 'success';
                $data['message'] = 'สำเร็จ!';
            } catch (\Exception $e) {
                \DB::rollBack();
                $data['status'] = 'error';
                $data['message'] = 'ไม่สำเร็จ!';
            }
        }else{
            $data['status'] = 'error';
            $data['message'] = 'ไม่สำเร็จ!';
        }
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Shift::insert($request->all())){
                    \DB::commit();
                    return "บันทึกสำเร็จ";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Shift::find($id)){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function show_shift_management($id)
    {
        try {
            $result = Shiftmanagement::select([
                'shift_management.*'
                ,\DB::raw('employee.firstname +\' \'+ employee.lastname as fullname')
                ,\DB::raw('shift.name as shift_name')
                ,\DB::raw('shift.time_start')
                ,\DB::raw('shift.time_end')
            ])
            ->leftjoin('employee','employee.id','shift_management.employee_id')
            ->leftjoin('shift','shift.id','shift_management.shift_id')
            ->find($id);
            if($result){
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            if( $result = Shift::where('id',$id)->update($request->all()) ){
                \DB::commit();
                return "อัพเดทข้อมูลสำเร็จ";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    public function update_shift_management(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try{
            Shiftmanagement::where( 'id' , $id )->update( $request->all() );
            \DB::commit();
            $data['status'] = 'success';
            $data['message'] = 'สำเร็จ!';
        } catch (\Exception $e) {
            \DB::rollBack();
            $data['status'] = 'error';
            $data['message'] = 'ไม่สำเร็จ!';
        }
        return $data;
    }

    public function delete_shift_management($id)
    {
        \DB::beginTransaction();
        try{
            Shiftmanagement::findOrFail($id)->delete();
            \DB::commit();
            $data['status'] = 'success';
            $data['message'] = 'สำเร็จ!';
        } catch (\Exception $e) {
            \DB::rollBack();
            $data['status'] = 'error';
            $data['message'] = 'ไม่สำเร็จ!';
        }
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Shift::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "ลบข้อมูลสำเร็จ";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}