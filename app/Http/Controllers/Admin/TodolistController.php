<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Todolist;
use App\Models\Employee;

class TodolistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['employee'] = Employee::get();

        $data['menu'] = 'Todolist';
        return view('admin.todolist')->with($data);
    }

    public function list()
    {
        $model = Todolist::query();
        $model->leftjoin('employee', 'todo_list.created_by', 'employee.id');

        $model->select(['employee.*','employee.id as employeeid','todo_list.*','todo_list.id as todo_listid']);
        return  \DataTables::eloquent($model)
                ->addColumn('action', function ($rec) {
                    $str = '
                        <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->todo_listid.'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->todo_listid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->id)) {
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if ($result = Todolist::insert($request->all())) {
                    \DB::commit();
                    return "บันทึกสำเร็จ";
                } else {
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e->getMessage();
            }
        } else {
            return $this->update($request, $request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if ($result = Todolist::find($id)) {
                return $result;
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            if ($result = Todolist::where('id', $id)->update($request->all())) {
                \DB::commit();
                return "อัพเดทข้อมูลสำเร็จ";
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Todolist::findOrFail($id);
        try {
            if ($example->delete()) {
                \DB::commit();
                return "ลบข้อมูลสำเร็จ";
            } else {
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e->getMessage();
        }
    }
}
