<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Employeeregistration;
use App\Models\Employee;
use App\Models\Company;
use App\Models\Branch;
use App\Models\Groups;
use App\Models\Department;


class EmployeeregistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['employee'] = Employee::get();
        $data['company'] = Company::get();
        $data['branch'] = Branch::get();
        $data['group'] = Groups::get();
        $data['department'] = Department::get();

        $data['menu'] = 'รายการลงทะเบียนทำงาน';
        return view('admin.employeeregistration')->with($data);
    }

    public function list(Request $request){
        $model = Employeeregistration::query();
        $model->leftjoin('employee','employee_registration.employee_id','employee.id');
        $model->select(['employee.firstname','employee.lastname','employee.id as employeeid','employee_registration.*','employee_registration.id as employee_registrationid']);
        if(isset($request->company_id)){
            $model->where('employee.company_id',$request->company_id);
        }
        if(isset($request->branch_id)){
            $model->where('employee.branch_id',$request->branch_id);
        }
        if(isset($request->group_id)){
            $model->where('employee.group_id',$request->group_id);
        }
        if(isset($request->department_id)){
            $model->where('employee.department_id',$request->department_id);
        }
        if(isset($request->employee_id)){
            $model->where('employee.id',$request->employee_id);
        }
        if(isset($request->in_date)){
            $model->whereBetween('employee_registration.in_date',[$request->in_date,$request->out_date]);
        }
        return  \DataTables::eloquent($model)
                ->addColumn('action',function($rec){
                    $str = '
                    <a class="btn btn-xs btn-warning btn-edit" href="#" data-id="'.$rec->employee_registrationid.'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-danger btn-delete" href="#" data-id="'.$rec->employee_registrationid.'">
                            <i class="fa fa-trash"></i>
                        </a>
                    ';
                    return $str;
                })
                ->editColumn('firstname',function($rec){
                    return $rec->firstname." ".$rec->lastname;
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->id)){
            $request['created_at'] = date("Y-m-d h:i:s");
            unset($request['id']);
            \DB::beginTransaction();
            try {
                if($result = Employeeregistration::insert($request->all())){
                    \DB::commit();
                    return "คุณเพิ่มข้อมูลสำเร็จ!";
                }else{
                    throw new \Exception('Error! Processing', 1);
                }
            } catch (\Exception $e) {
                \DB::rollBack();
                return $e;
            }
        }else{
            return $this->update($request,$request->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            if($result = Employeeregistration::find($id)){
                $date1 = date_create($result['in_time']);
                $date1 = date_format($date1,"H:i:s");
                $result['in_time'] = $date1;
                $date2 = date_create($result['out_time']);
                $date2 = date_format($date2,"H:i:s");
                $result['out_time'] = $date2;
                return $result;
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        unset($request['id']);
        \DB::beginTransaction();
        try {
            unset($request['id']);
            if($result = Employeeregistration::where('id',$id)->update($request->all())){
                \DB::commit();
                return "คุณอัพเดทข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        $example = Employeeregistration::findOrFail($id);
        try {
            if($example->delete()){
                \DB::commit();
                return "คุณลบข้อมูลสำเร็จ!";
            }else{
                throw new \Exception('Error! Processing', 1);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e;
        }
    }
}