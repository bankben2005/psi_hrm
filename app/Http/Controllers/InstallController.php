<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InstallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $data['menu'] = 'Install';
        return view('install.index')->with($data); // install/index
    }

    public function list(){
        return \DataTables::of(\DB::select('SHOW TABLES'))
        ->addIndexColumn()
        ->toJson();
    }

    public function column(Request $request){
        return \DataTables::of(\DB::select('SHOW COLUMNS FROM '.$request->table))
        ->addIndexColumn()
        ->addColumn('checkall',function($rec){
        return      '<div class="form-check">
                        <input type="checkbox" class="form-check-input" name="'.$rec->Field.'[use]">
                        <label class="form-check-label"></label>
                    </div>
                    <input type="hidden" name="'.$rec->Field.'[field]" value="'.$rec->Field.'">'
                    ;
        })
        ->addColumn('inputtype',function($rec){
        $str = 
        '<select class="inputtype" name="'.$rec->Field.'[inputtype]">
            <option value="">== select input type ==</option>
            <option value="text">text</option>
            <option value="email">email</option>
            <option value="password">password</option>
            <option value="dropdown">dropdown</option>
            <option value="checkbox">checkbox</option>
            <option value="radio">radio</option>
            <option value="date">date</option>
            <option value="time">time</option>
            <option value="textarea">textarea</option>
            <option value="status">status</option>
        </select>';
        return $str;
        })
        ->addColumn('validate',function($rec){
            return '<input name="'.$rec->Field.'[validate]" type="checkbox">';
        })
        ->addColumn('null',function($rec){
            return null;
        })
        ->rawColumns(['checkall','inputtype','validate'])
        ->make(true);
    }

    public function detailvalue(Request $request){
        if(count($request->all())==0){
            return \DB::select('SHOW TABLES');
        }else if(isset($request->table)){
            return \DB::select('SHOW COLUMNS FROM '.$request->table);
        }else{
            return false;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {  
        try {
            $table = $request->tablename;
            $ucfirst = str_replace("_",'',ucfirst($table));
            $lower = str_replace("_",'',strtolower($table));

            $result['model'] = Storage::disk('models')->delete($ucfirst.".php");
            $result['view'] = Storage::disk('views')->delete($ucfirst.".blade.php");
            $result['controller'] = Storage::disk('controllers')->delete($ucfirst."Controller.php");
            $result['js'] = Storage::disk('asset_js')->delete($lower.".js");

            $content = Storage::disk('routes')->get('web.php');
            $content = str_replace('Route::get(\''.$lower.'/list\',\'Admin\\'.$ucfirst.'Controller@list\');','',$content);
            $content = str_replace('Route::resource(\''.$lower.'\',\'Admin\\'.$ucfirst.'Controller\');','',$content);
            $content = Storage::disk('routes')->put("web.php",$content);

            \App\AdminMenu::where('menu_url', 'like', "%admin/$lower%")->delete();

            $string = "ลบ ";
            foreach ($result as $key => $value) {
                $string .= "$key ";
            }
            $string .= "สำเร็จ";
            return $string;
        } catch (\Exception $th) {
            return $th;
        }
    }

    public function createmvc(Request $request){
        // return $request->all();
        foreach ($request->all() as $key => $value) {
            if(!empty($value['source'])){
                $result['model_'.$value['source']] = $this->createmodel($value['source']);
            }
        }

        $result['model'] = $this->createmodel($request->table);
        $result['route'] = $this->createroute($request);
        $result['controller'] = $this->createcontroller($request);
        $result['createjs'] = $this->createjs($request);
        $result['view'] = $this->createview($request);
        $input['menu_url'] = "admin/".str_replace("_",'',strtolower($request->table));
        $input['menu_name'] = str_replace("_",'',ucfirst($request->table));
        $input['menu_icon'] = "fa-cog";
        $input['main_menu'] = 0;
        $input['created_at'] = date('Y-m-d H:i:s');
        $result['result'] = ( \App\AdminMenu::insert($input) )? "สร้างระบบ ".str_replace("_",'',ucfirst($request->table))." สำเร็จ!" :"พบข้อผิดผลาด!";
        return $result;
    }

    public function createroute(Request $request){
        $str = '';
        if($request){
            if($request->table){
                $path = str_replace("_",'',strtolower($request->table));
                $pathCtrl = str_replace("_",'',ucfirst($request->table));
                $str .= 'Route::get(\''.$path .'/list\',\'Admin\\'.$pathCtrl.'Controller@list\');'."\n";
                $str .= "\t".'Route::resource(\''.$path .'\',\'Admin\\'.$pathCtrl.'Controller\');'."\n";
                $str .= "\t".'/*==Addition_Route==*/';
            }
        }
        $content = Storage::disk('routes')->get('web.php');
        $content = str_replace('/*==Addition_Route==*/',$str,$content);
        //write new model file
        if( Storage::disk('routes')->put("web.php",$content) ){
            return true;
        }else{
            return false;
        } 
    }

    public function createmodel($table){
        $classname = str_replace("_",'',ucfirst($table));
        if(!file_exists(app_path("Models/$classname.php"))){

            //read examplemodel file
            $content = Storage::disk('install')->get('example_model.txt');
            $content = str_replace("#modelclass#",$classname,$content);
            $modeltable = 'protected $table = \''.$table.'\';';
            $content = str_replace("#modeltable#",$modeltable,$content);

            //write new model file
            if(Storage::disk('models')->put("$classname.php",$content)){
                return true;
            }else{
                return false;
            } 
        }else{
            return 'Already has '."$classname.php".' file';
        }
    }

    public function createcontroller(Request $request){
        $table = $request->table;
        $classname = str_replace("_",'',ucfirst($table));
        if(!file_exists(app_path("Http/Controllers/Admin/".$classname."Controller.php"))){
            $content = Storage::disk('install')->get('example_controller.txt');

            #controllerclass#
            $content = str_replace("#controllerclass#",$classname.'Controller',$content);

            #usemainmodel#
            $usemainmodel = 'use App\Models\\'.$classname.';';
            if(empty(strpos($content,$usemainmodel))){
                $content = str_replace("#usemainmodel#",$usemainmodel,$content);
            }

            #usechildmodel#
            #childdata#
            $usechildmodel = ''; $childdata = ''; $jointable = ''; $select = null;
            $check['use'] = array(); $check['childdata'] = array(); $check['join'] = array();
            foreach ($request->all() as $key => $value) {
                if(!empty($value['source'])){
                    if(!in_array($value['source'], $check['use'])){
                        $usechildmodel .= "use App\Models\\".str_replace("_",'',ucfirst($value['source'])).";\r\n";
                        $check['use'][] = $value['source'];
                    }
                    if(!in_array($value['source'], $check['childdata'])){
                        $childdata .= "\t\t".'$data[\''.str_replace("_",'',strtolower($value['source'])).'\'] = '.str_replace("_",'',ucfirst($value['source'])).'::get();'."\n";
                        $check['childdata'][] = $value['source'];
                    }
                    if(!in_array($value['source'], $check['join'])){
                        $jointable .= '$model->leftjoin(\''.strtolower($value['source']).'\',\''.$request->table.'.'.$value['field'].'\',\''.$value['source'].'.'.$value['id'].'\');'."\n";
                        $check['join'][] = $value['source'];
                    }
                    $select[] = \App\Http\Controllers\FunctionController::GetPK($value['source']);
                }
            }
            
            $select[] = \App\Http\Controllers\FunctionController::GetPK($request->table);
            $select = \App\Http\Controllers\FunctionController::StringConcat($select,',');
            #selectcolumn#
            $content = str_replace("#selectcolumn#",$select,$content);

            #maintableprimary#
            $content = str_replace("#maintableprimary#",\App\Http\Controllers\FunctionController::GetTablePrimary($request->table),$content);
            
            $content = str_replace("#usechildmodel#",$usechildmodel,$content);

            $content = str_replace("#childdata#",$childdata,$content);

            #mainmodel#
            $content = str_replace("#mainmodel#",$classname,$content);

            #mainmodelstrtolower#
            $content = str_replace("#mainmodelstrtolower#",str_replace("_",'',strtolower($request->table)),$content);

            #jointable#
            // $model->leftjoin('users','examples.created_by','=','users.id');
            $content = str_replace("#jointable#",$jointable,$content);

            if(Storage::disk('app')->put("Http/Controllers/Admin/".$classname ."Controller.php",$content)){
                return true;
            }else{
                return false;
            } 
        }else{
            return 'Already has '.$classname."Controller.php".' file';
        }
    }

    public function createjs(Request $request){
        $table = $request->table;
        $classname = str_replace("_",'',ucfirst($table));
        $lowerclassname = str_replace("_",'',strtolower($table));
        $lower = strtolower($table);
        if(!file_exists(public_path("assets/admin/js/admin/$lowerclassname.js"))){
            //read examplemodel file
            $content = Storage::disk('install')->get('example_js.txt');
            $content = str_replace("#strlower#",$lowerclassname,$content);
            $content = str_replace("#classname#",$classname,$content);
            
            //write new model file
            $str = ''; $editfield = ''; $validate = '';
            foreach ($request->all() as $key => $value) {
                if(!empty($value['use'])){
                    if(!empty($value['use'])){
                        if(!empty($value['source'])){
                            $str.="\t\t\t\t\t{\"data\":\"".$value['name']."\",\"name\":\"".$value['source'].".".$value['name']."\"},\n";
                        }else{
                            $str.="\t\t\t\t\t{\"data\":\"$key\",\"name\":\"$lower."."$key\"},\n";
                        }
                    }
                }

                if(!empty($value['validate']) && $value['validate']=="on"){
                    $validate .= "\t\t\t\t\t".$value['field'].": {required: true},\n";
                }
            }
            $content = str_replace("#showfield#",$str,$content);
            $content = str_replace("#validatefield#",$validate,$content);

            if(Storage::disk('asset_js')->put("$lowerclassname.js",$content)){
                return true;
            }else{
                return false;
            }   
        }else{
            return 'Already has '."$lowerclassname.js".' file';
        }
    }

    public function createview(Request $request){
        $table = $request->table;
        $lowerclassname = str_replace("_",'',strtolower($table));
        if(!file_exists(resource_path("views/admin/$lowerclassname.blade.php"))){
            $content = Storage::disk('install')->get('example_view.txt');
            $str = ''; $datatablefield = '';
            foreach ($request->all() as $key => $value) {
                if(!empty($value['use'])){
                    $str .= $this->createform(array(
                        'field' => !empty($value['field']) ? $value['field'] : null
                        ,'id' => !empty($value['id']) ? $value['id'] : null
                        ,'name' => !empty($value['name']) ? $value['name'] : null
                        ,'type' => !empty($value['inputtype']) ? $value['inputtype'] : null
                        ,'source'=> !empty($value['source']) ? $value['source'] : null
                    ));

                    $datatablefield .= "\t\t\t\t\t\t\t\t".'<th>'.(!empty($value['name']) ? $value['name'] : '').'</th>';
                    $datatablefield .= "\n";
                }
            }
            // return $datatablefield;
            $content = str_replace("#datatablefield#",$datatablefield,$content);
            $content = str_replace("#jsfile#",$lowerclassname,$content);
            $content = str_replace("#forminput#",$str,$content);
            $content = str_replace("#iddatatable#",$lowerclassname,$content);
            if(Storage::disk('views')->put("$lowerclassname.blade.php",$content)){
                return true;
            }else{
                return false;
            }  
        }else{
            return 'Already has '."$lowerclassname.js".' file';
        }
    }

    public function createform($value){

        $id = $value['id'];
        $name = $value['name'];
        $type = $value['type'];
        $source = $value['source'];
        $field = $value['field'];

        switch ($type) {
            case 'text':
            return 
            '<div class="form-group row">
                '."\t\t\t".'<label for="'.$name.'" class="col-sm-2 col-form-label">'.$name.'</label>
                '."\t\t\t".'<div class="col-sm-10">
                    '."\t\t\t".'<input type="'.$type.'" name="'.$name.'" placeholder="'.$name.'" class="form-control input-sm">
                '."\t\t\t".'</div>
            '."\t\t\t".'</div>'."\n";
            break;

            case 'email':
            return
            '<div class="form-group row">
                '."\t\t\t".'<label for="'.$name.'" class="col-sm-2 col-form-label">'.$name.'</label>
                '."\t\t\t".'<div class="col-sm-10">
                    '."\t\t\t".'<input type="'.$type.'" name="'.$name.'" placeholder="'.$name.'" class="form-control input-sm">
                '."\t\t\t".'</div>
            '."\t\t\t".'</div>'."\n";
            break;

            case 'password':
            return
            '<div class="form-group row">
                '."\t\t\t".'<label for="'.$name.'" class="col-sm-2 col-form-label">'.$name.'</label>
                '."\t\t\t".'<div class="col-sm-10">
                    '."\t\t\t".'<input type="'.$type.'" name="'.$name.'" placeholder="'.$name.'" class="form-control input-sm">
                '."\t\t\t".'</div>
            '."\t\t\t".'</div>'."\n";
            break;
            
            case 'textarea':
            return
            "\t\t\t\t\t\t".'<div class="form-group row">
                '."\t\t\t".'<label for="'.$name.'" class="col-sm-2 col-form-label">'.$name.'</label>
                '."\t\t\t".'<div class="col-sm-10">
                    '."\t\t\t".'<textarea name="'.$name.'" class="form-control input-sm"></textarea>
                '."\t\t\t".'</div>
            '."\t\t\t".'</div>'."\n";
            break;

            case 'date':
            return
            "\t\t\t\t\t\t".'<div class="form-group row">
                '."\t\t\t".'<label for="'.$name.'" class="col-sm-2 col-form-label">'.$name.'</label>
                '."\t\t\t".'<div class="col-sm-10">
                    '."\t\t\t".'<input type="'.$type.'" name="'.$name.'" placeholder="'.$name.'" class="form-control input-sm">
                '."\t\t\t".'</div>
            '."\t\t\t".'</div>'."\n";
            break;

            case 'time':
            return
            '<div class="form-group row">
                '."\t\t\t".'<label for="'.$name.'" class="col-sm-2 col-form-label">'.$name.'</label>
                '."\t\t\t".'<div class="col-sm-10">
                    '."\t\t\t".'<input type="'.$type.'" name="'.$field.'" placeholder="'.$name.'" class="form-control input-sm">
                '."\t\t\t".'</div>
            '."\t\t\t".'</div>'."\n";
            break;

            case 'dropdown':
                $str = '';
            if(!empty($value['source']) && !empty($value['id']) && !empty($value['name'])){
                $str .= "\t\t\t\t\t\t".'<div class="form-group row">'."\n";
                $str .= "\t\t\t\t\t\t".'    <label for="'.$name.'" class="col-sm-2 col-form-label">'.$name.'</label>'."\n";
                $str .= "\t\t\t\t\t\t".'    <div class="col-sm-10">'."\n";
                $str .= "\t\t\t\t\t\t".'    <select class="ls-select2" name="'.$field.'">'."\n";
                $str .= "\t\t\t\t\t\t".'        <option value="">== '.$name.' ==</option>'."\n";
                $str .= 
                "\t\t\t\t\t\t\t\t".'@foreach ($'.$source.' as $key => $item)
                '."\t\t\t\t".'<option value="{{$item->'.$id.'}}">{{$item->'.$name.'}}</option>
                '."\t\t\t\t".'@endforeach'."\n";
                $str .= "\t\t\t\t\t\t\t".'</select>'."\n";
                $str .= "\t\t\t\t\t\t\t".'</div>'."\n";
                $str .= "\t\t\t\t\t\t".'</div>'."\n";
                return $str;
            }
            break;

            case 'status':
                $str = '';
                $str .= "\t\t\t\t\t\t".'<div class="form-group row">'."\n";
                $str .= "\t\t\t\t\t\t".'    <label for="'.$name.'" class="col-sm-2 col-form-label">'.$name.'</label>'."\n";
                $str .= "\t\t\t\t\t\t".'    <div class="col-sm-10">'."\n";
                $str .= "\t\t\t\t\t\t".'    <select class="ls-select2" name="'.$field.'">'."\n";
                $str .= "\t\t\t\t\t\t".'        <option value="">== สถานะ ==</option>'."\n";
                $str .= "\t\t\t\t\t\t".'        <option value="T">== เปิดใช้งาน ==</option>'."\n";
                $str .= "\t\t\t\t\t\t".'        <option value="F">== ยกเลิก ==</option>'."\n";
                $str .= "\t\t\t\t\t\t\t".'</select>'."\n";
                $str .= "\t\t\t\t\t\t\t".'</div>'."\n";
                $str .= "\t\t\t\t\t\t".'</div>'."\n";
                return $str;
            break;

            case 'checkbox':
            if(!empty($value['source']) && !empty($value['id']) && !empty($value['name'])){

                $str = "\t\t\t\t\t\t". '<div class="form-group row">';
                $str .= "\t\t\t\t\t\t".'<label for="meu_name" class="col-sm-2 col-form-label">'.$field.'</label>'."\n";
                $str .= 
                "\t\t\t\t\t\t".'@foreach ($'.$source.' as $key => $item)
                    '."\t".'<div class="form-check">
                    '."\t\t".'<input type="'.$type.'" value="{{$item->'.$id.'}}" name="'.$field.'[]" class="" id="{{$item->'.$name.'}}">
                    '."\t\t".'<label for="{{$item->'.$name.'}}" class="form-check-label">{{$item->'.$name.'}}</label>
                    '."\t".'</div>
                '."\t\t".'@endforeach'
                .'</div>';
                return $str;
            }
            break;

            case 'radio':
            if(!empty($value['source']) && !empty($value['id']) && !empty($value['name'])){

                $str = "\t\t\t\t\t\t". '<div class="form-group row">';
                $str .= "\t\t\t\t\t\t".'<label for="meu_name" class="col-sm-2 col-form-label">'.$field.'</label>'."\n";
                $str .= 
                "\t\t\t\t\t\t".'@foreach ($'.$source.' as $key => $item)
                    '."\t".'<div class="form-check">
                    '."\t\t".'<label for="'.$name.'" class="form-check-label"><input type="'.$type.'" value="{{$item->'.$id.'}}" name="'.$field.'" class="" id="{{$item->'.$name.'}}">{{$item->'.$name.'}}</label>
                    '."\t".'</div>
                '."\t\t".'@endforeach'
                .'</div>';
                return $str;
            }
            break;
        }
    }

}
