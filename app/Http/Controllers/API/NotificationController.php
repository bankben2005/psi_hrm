<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use stdClass;
use AUTH;

use OneSignal;

use App\Models\Notification;

class NotificationController extends Controller
{
    public static $app_id = '828752d9-0826-4e12-9453-80f76dfba0ac';
    public static $app_rest_key = 'YWI0YWNiNTYtOWNmYi00ZjBiLThlMTEtMmM4ZGM3M2FmMzNi';

    public function index()
    {
        return view('home');
    }

    public function Push(Request $request)
    {
        $return = array();
        $content      = array(
            "en" => 'ข้อความภาษาไทย'
        );
        $hashes_array = array();
        array_push($hashes_array, array(
            "id" => "282",
            "text" => "content to Dev segment",
            "icon" => "http://i.imgur.com/N8SN8ZS.png",
            "url" => "http://hrmsystem.psisat.com"
        ));
        $fields = array(
            'app_id' => "828752d9-0826-4e12-9453-80f76dfba0ac",
            'included_segments' => array(
                'All'
            ),
            'data' => array(
                "function" => "Push"
            ),
            'contents' => $content,
            'web_buttons' => $hashes_array
        );
        $fields = json_encode($fields);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic YWI0YWNiNTYtOWNmYi00ZjBiLThlMTEtMmM4ZGM3M2FmMzNi'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $response = curl_exec($ch);
        curl_close($ch);
        $return['fields'] = $fields;
        $return['response'] = $response;
        return $return;
    }

    public static function addDevice(Request $request)
    {
        $deviceType = array(
            'iOS'=>'0',
            'android'=>'1',
            'amazon'=>'2',
            'windowsPhone'=>'3',
            'whromeApps'=>'4',
            'whromeWebPush'=>'5',
            'windows'=>'6',
            'safari'=>'7',
            'firefox'=>'8',
            'macOS'=>'9',
            'alexa'=>'10',
            'email'=>'11',
            'ios'=>'0',
        );
        // return $deviceType[ $request['os'] ];
        $fields = array(
            'app_id' => static::$app_id,
            'identifier' => $request['token_device'],
            'language' => "en",
            'device_type'=> $deviceType[ $request['os'] ],
            // 'timezone' => "-28800",
            // 'game_version' => "1.0",
            // 'device_os' => "9.1.3",
            // 'device_model' => "iPhone 8,2",
            // 'tags' => array("foo" => "bar")
        );
        
        $fields = json_encode($fields);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/players");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $response = curl_exec($ch);
        curl_close($ch);
        
        $return = json_decode($response);
        // return response()->json($return);
        return ($return);
    }

    public static function pushNotification($params)
    {
        // return $params['data'];
        $data_key = array();
        // foreach ($params['employee_id'] as $key => $value) {
        //     $data_key[] = md5($value.(int)$params['data']['id'].$params['data']['type'].((string)date('Y-m-d H:i:s')));
        // }
        $log_insert = array();
 

        $wording = array();
        $wording['type'] = isset($params['type']) ? $params['type'] : '';
        $wording['content'] = isset($params['content']) ? $params['content'] : '';
        $wording['task'] = isset($params['task']) ? $params['task'] : '';
        // return $wording;
        $content = static::wording($wording);

        // start set log_insert
        foreach ($params['employee_id'] as $key => $value) {
            $key_in = md5($value.(int)$params['data']['id'].$params['data']['type'].((string)date('Y-m-d H:i:s')));

            $log['employee_id'] = $value;
            $log['type'] = $params['data']['type'];
            $log['id'] = (int)$params['data']['id'];
            $log['content'] = $content['content'];
            $log['title'] = $content['headings'];
            $log['status'] = 'N';
            $log['created_at'] = date('Y-m-d H:i:s');
            $log['created_by'] = $params['created_by'];
            $log['key'] = $key_in;
            $log_insert[] = $log;

            $data_key[] = $key_in;
        }
        // end set log_insert

        $request = new Request;
        $request['headings'] = isset($content['headings']) ? $content['headings'] : '';
        $request['content'] = isset($content['content']) ? $content['content'] : '';
        $request['userId'] = count($params['userId'])!=0 ? $params['userId'] : [];
        $params['data']['key'] = $data_key;
        $params['data']['parameter'] = static::parameter_noti($params['data']);
        $request['data'] = count($params['data'])!=0 ? $params['data'] : [];
        $result = static::sendpushCustom($request);
        if ($result['message']=='success') {
            if (isset($params['employee_id'])) {
                Notification::insert($log_insert);
            }
        }
        return $result;
    }

    public static function sendpushCustom(Request $request)
    {
        // return $request;
        $content = array(
            "en" => $request['content'],
            "th" => $request['content']
            );
        $headings = array(
            "en" => $request['headings'],
            "th" => $request['headings']
            );
        $fields = array(
            'app_id' => static::$app_id,
            'include_player_ids' => is_array($request['userId']) ? $request['userId'] : array($request['userId']),
            'data' => $request['data'],
            'ios_badgeCount'=> 1,
            'ios_badgeType'=> 'Increase',
            'contents' => $content,
            'headings' => $headings
        );

        $return = array();
        $result = array();
        
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic '.static::$app_rest_key
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        try {
            $response = curl_exec($ch);
            curl_close($ch);
            $return = ["type"=>"notifications","message"=>"success","return"=>($response)];
        } catch (\Throwable $th) {
            $return = ["type"=>"notifications","message"=>"false","return"=>($response),"exception"=>json_decode($th)];
        }
        return $return;
    }

    public static function wording($params = null)
    {
        switch ($params['type']) {
            case 'news_create':
                //noti ประชาสัมพันธ์
                $obj['headings'] = "ประชาสัมพันธ์";
                $obj['content'] = $params['content'];
                $return = $obj;
                break;
            case 'leave_request':
                //noti หลังจากอนุมัติ / ไม่อนุมัติแล้ว
                $obj['headings'] = "การลา";
                $obj['content'] = 'มีรายการลาจากพนักงาน กรุณาตรวจสอบเพื่อทำการ อนุมัติหรือไม่อนุมัติ';
                $return = $obj;
                break;

            case 'leave_result':
                //noti หลังจากอนุมัติ / ไม่อนุมัติแล้ว
                $obj['headings'] = 'การลา';
                $obj['content'] = 'ผลการอนุมัติได้ถูกอัพเดท! เปิดเพื่อดูรายละเอียด';
                $return = $obj;
                break;

            case 'project_create':
                //noti เมื่อเข้าร่วมโปรเจค / แก้ไข
                $obj['headings'] = 'โปรเจค';
                $obj['content'] = 'คุณได้เข้าร่วมโปรเจค '.(isset($params['content']) ? $params['content'] : "").' เปิดเพื่อดูรายละเอียด';
                $return = $obj;
                break;

            case 'project_update':
                //noti โปรเจคเมื่ออัพเดทโปรเจค / เปลี่ยนสถานะงาน
                $obj['headings'] = 'โปรเจค';
                $content = isset($params['content']) ? $params['content'] : '';
                $obj['content'] = 'พบการอัพเดทโปรเจค'.$content.' เปิดเพื่อดูรายละเอียด';
                $return = $obj;
                break;

            case 'project_task_create':
                //noti โปรเจคเมื่อ สร้าง task งาน / เปลี่ยนสถานะงาน
                $obj['headings'] = 'โปรเจค';
                $content = isset($params['content']) ? $params['content'] : '';
                $task = isset($params['task']) ? $params['task'] : '';
                $obj['content'] = 'โปรเจค '.$content.' มีการอัพเดทงาน '.$task.' เปิดเพื่อดูรายละเอียด';
                $return = $obj;
                break;

            case 'project_task_update':
                //noti โปรเจคเมื่อ สร้าง task งาน / เปลี่ยนสถานะงาน
                $obj['headings'] = 'โปรเจค';
                $content = isset($params['content']) ? $params['content'] : '';
                $task = isset($params['task']) ? $params['task'] : '';
                $obj['content'] = 'โปรเจค '.$content.' มีการอัพเดทงาน '.$task.' เปิดเพื่อดูรายละเอียด';
                $return = $obj;
                break;

            default:
                $return['status'] = false;
                break;
        }
        return $return;
    }

    public static function pushNotificationAll($params)
    {
        //    return $params;
        $wording = array();
        $wording['type'] = $params['type'];
        $wording['content'] = $params['content'];
        $content = static::wording($wording);
        $request = new Request;
        $request['headings'] = isset($content['headings']) ? $content['headings'] : '';
        $request['content'] = isset($content['content']) ? $content['content'] : '';
        $key = md5((int)$params['data']['news_id'].$params['data']['type'].((string)date('Y-m-d H:i:s')));
        $param['data']['key'] = $key;
        $request['data'] = count($params['data'])!=0 ? $params['data'] : [];
        $result = static::sendpushAll($request);
        if ($result['message']=='success') {
            $log_insert = array();
            $log['employee_id'] = 0;
            $log['type'] = $params['data']['type'];
            $log['id'] = (int)$params['data']['news_id'];
            $log['content'] = $content['content'];
            $log['title'] = $content['headings'];
            $log['status'] = 'N';
            $log['created_at'] = date('Y-m-d H:i:s');
            $log['created_by'] = AUTH::user()->employee_id;
            $log['key'] = $key;
            $log_insert[] = $log;
            Notification::insert($log_insert);
        }
        return $result;
    }

    public static function sendpushAll(Request $request)
    {
        $content = array(
            "en" => $request['content'],
            "th" => $request['content']
        );
        $headings = array(
            "en" => $request['headings'],
            "th" => $request['headings']
        );

        $fields = array(
            'app_id' => static::$app_id,
            'included_segments' => array(
                'All'
            ),
            'data' => $request['data'],
            
            'ios_badgeCount'=> 1,
            'ios_badgeType'=> 'Increase',
            

            'contents' => $content,
            'headings' => $headings,
        );
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic '.static::$app_rest_key
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        try {
            $response = curl_exec($ch);
            curl_close($ch);
            $return = ["type"=>"notifications","message"=>"success","return"=>($response)];
        } catch (\Throwable $th) {
            $return = ["type"=>"notifications","message"=>"false","return"=>($response),"exception"=>json_decode($th)];
        }
        return $return;
    }

    public function get_log_notification()
    {
        $login_user = AUTH::user()->employee_id;
        $obj_return = new stdClass();
        $result =  Notification::select([
            'notification.key'
            ,'notification.id'
            ,'notification.type'
            ,'notification.title'
            ,'notification.content'
            ,'notification.status'
            ,'notification.created_at'
            ,'notification.created_by'
            ,'notification.employee_id'
            ,\DB::raw('(SELECT project_task.project_id FROM project_task WHERE project_task.id=notification.id) as project_id')
        ])
        ->where('employee_id', $login_user)
        ->orWhere('employee_id', '0')
        ->take(10)
        ->orderBy('created_at', 'desc')
        ->get();
        if ($result) {
            // $return = array();
            foreach ($result  as $key => $value) {
                $parameter = array();
                switch ($value->type) {
                    case 'leave_request':
                        $parameter['leave_id'] = "$value->id";
                        $parameter['employee_id'] = $value->employee_id;
                    break;

                    case 'leave_result':
                        $parameter['leave_id'] = "$value->id";
                        $parameter['employee_id'] = $value->employee_id;
                    break;

                    case 'project_create':
                        $parameter['project_id'] = "$value->id";
                        $parameter['employee_id'] = $value->employee_id;
                    break;

                    case 'project_update':
                        $parameter['project_id'] = "$value->id";
                        $parameter['employee_id'] = $value->employee_id;
                    break;

                    case 'project_task_create':
                        $parameter['project_id'] = $value->project_id;
                        $parameter['project_task_id'] = "$value->id";
                        $parameter['employee_id'] = $value->employee_id;
                    break;

                    case 'project_task_update':
                        $parameter['project_id'] = "$value->project_id";
                        $parameter['project_task_id'] = "$value->id";
                        $parameter['employee_id'] = $value->employee_id;
                    break;

                    case 'news_create':
                        $parameter['news_id'] = "$value->id";
                    break;
                }
                $result[$key]->parameter = $parameter;
            }
            $obj_return->result_code = "000";
            $obj_return->result_desc = "Completed";
            $obj_return->status = true;
            $obj_return->data = $result;
        } else {
            $obj_return->result_code = "002";
            $obj_return->result_desc = "NotFoundDataException";
            $obj_return->status = false;
            $obj_return->data = [];
        }
        return response()->json($obj_return);
    }

    public function update_log_notification(Request $request)
    {
        // return $request->all();
        $login_user = AUTH::user()->employee_id;
        $insert['status'] = $request->status;
        $obj_return = new stdClass();

        try {
            $result =  Notification::where([
                ['employee_id',$login_user]
            ])
            ->whereIn('key', $request->key)
            ->update($insert);
            $obj_return->result_code = "000";
            $obj_return->result_desc = "Completed";
            $obj_return->status = true;
        } catch (\Throwable $th) {
            $obj_return->result_code = "002";
            $obj_return->result_desc = "NotFoundDataException";
            $obj_return->status = false;
            $obj_return->exception = $th->getTrace();
        }
        return response()->json($obj_return);
    }

    public function count_noti_unread()
    {
        $login_user = AUTH::user()->employee_id;
        $obj_return = new stdClass();
        if ($result =  Notification::where('employee_id', $login_user)->take(10)->orderBy('created_at', 'desc')->get()) {
            $unread = 0;
            $data['unread'] = 0;
            foreach ($result as $key => $value) {
                if ($value->status=='N') {
                    $unread++;
                }
                $data['unread'] = $unread;
            }
            $obj_return->result_code = "000";
            $obj_return->result_desc = "Completed";
            $obj_return->status = true;
            $obj_return->data = $data;
        } else {
            $obj_return->result_code = "002";
            $obj_return->result_desc = "NotFoundDataException";
            $obj_return->status = false;
            $obj_return->data = [];
        }
        return response()->json($obj_return);
    }

    public static function parameter_noti($params)
    {
        // return $params['id'];
        // $parameter = array();
        switch ($params['type']) {
            case 'leave_request':
                $parameter['leave_id'] = $params['id'];
                // $parameter['employee_id'] = $params['employee_id'];
            break;

            case 'leave_result':
                $parameter['leave_id'] = $params['id'];
                // $parameter['employee_id'] = $params['employee_id'];
            break;

            case 'project_create':
                $parameter['project_id'] = $params['id'];
                // $parameter['employee_id'] = $params['employee_id'];
            break;

            case 'project_update':
                $parameter['project_id'] = $params['id'];
                // $parameter['employee_id'] = $params['employee_id'];
            break;

            case 'project_task_create':
                $parameter['project_id'] = $params['project_id'];
                $parameter['project_task_id'] = $params['id'];
                // $parameter['employee_id'] = $params['employee_id'];
            break;

            case 'project_task_update':
                $parameter['project_id'] = $params['project_id'];
                $parameter['project_task_id'] = $params['id'];
                // $parameter['employee_id'] = $params['employee_id'];
            break;

            case 'news_create':
                $parameter['news_id'] = $params['id'];
            break;
        }
        return $parameter;
    }
}
