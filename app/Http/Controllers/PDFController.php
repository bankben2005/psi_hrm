<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

use App\Models\Employee;
use App\Models\Employeeleave;
use App\Models\Employeeexperience;

use App\Http\Controllers\Admin\ReportController as ReportController;
use App\Http\Controllers\FunctionController as FunctionController;

class PDFController extends Controller
{
    public function employee_conclusion(Request $request){
        $test = new \DateTime("$request->month-01 00:00:00");
        $month = date_format($test, 'Y-m');
        $request['date_start'] = date_format($test, 'Y-m-d');
        $request['date_end'] = $month.date_format($test, '-t');

        $data['title'] = "รายงานการลงเวลาทำงาน";
        $data['request'] = $request->all();
        $data['employee'] = ReportController::report_employee_detail($request->employee_id);
        $data['employee_registration'] = [];
        $registration =  ReportController::employee_registration($request);
        foreach($registration as $key => $value){
            $value->hr = \App\Http\Controllers\FunctionController::convert_minute_hr(intval($value->hr));
            $value->hr = ($value->hr==NULL) ? '-' : $value->hr;
            $data['employee_registration'][] = $value;
        }
        $data['leave_conclusion'] = ReportController::leave_conclusion($request);
        $data['employee_confirm'] = ReportController::check_confirm($request);;

        try {
            $pdf = PDF::loadView('pdf.employee_conclusion',$data);
            return @$pdf->stream();
        }catch(\Throwable $th){
            return $th->getMessage();
        }
        
    }

    public function employee_leave(Request $request){
        $data['data'] = Employeeleave::where('employee_leave.id',$request->id)
        ->select([
            'employee_leave.*'
            ,\DB::raw('em.firstname + \' \' + em.lastname as name')
            ,'em.firstname'
            ,'em.lastname'
            ,'em.empcode as employee_code'
            ,'em.email'
            ,'company.name as cname'
            ,'branch.branch_name as bname'
            ,'groups.name as gname'
            ,'department.name as dname'
            ,'level.name as lname'
            ,'leave_type.leave_name as ltname'
            ,'em.picture_profile'
            ,\DB::raw('ap.firstname + \' \' + ap.lastname as apname')
        ])
        ->leftjoin(\DB::raw('employee em'),'em.id','employee_leave.employee_id')
        ->leftjoin(\DB::raw('employee ap'),'ap.id','employee_leave.approver_id')
        ->leftjoin('company','em.company_id','company.id')
        ->leftjoin('branch','em.branch_id','branch.id')
        ->leftjoin('groups','em.group_id','groups.id')
        ->leftjoin('department','em.department_id','department.id')
        ->leftjoin('level','em.level_id','level.id')
        ->leftjoin('leave_type','leave_type.id','employee_leave.leave_type_id')
        ->first();

        $data['title'] = "ข้อมูลการลา";
        $data['data_date'] = date('Y-m-d');
        $leave_result = ['T'=>'อนุมัติ','F'=>'ไม่อนุมัติ','N'=>'รออนุมัติ'];
        $data['data']->leave_result = $leave_result[ ($data['data']->leave_result) ];

        try {
            $pdf = PDF::loadView('pdf.employee_leave',$data);
            return @$pdf->stream();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
        
    }

    public function employee_query(Request $request){
        $model = Employee::query();
        $model->leftjoin('branch','employee.branch_id','branch.id');
        $model->leftjoin('company','employee.company_id','company.id');
        $model->leftjoin('groups','employee.group_id','groups.id');
        $model->leftjoin('department','employee.department_id','department.id');
        $model->leftjoin('level','employee.level_id','level.id');
        $model->leftjoin('employee_status','employee.employee_status_id','employee_status.id');
        $model->leftjoin('employee_level','employee.employee_level_id','employee_level.id');
        $model->leftjoin('hospitals','employee.hospital_id','hospitals.id');
        $model->leftjoin('education_level','employee.education_level_id','education_level.id');
        $model->orderBy('employee.empcode','ASC');
        if(!empty($request->employee_id)){
            $model->where(\DB::raw('employee.id'),$request->employee_id);
        }
        if(!empty($request->companies)){
            $model->where(\DB::raw('employee.company_id'),$request->companies);
        }
        if(!empty($request->branches)){
            $model->where(\DB::raw('employee.branch_id'),$request->branches);
        }
        if(!empty($request->levels)){
            $model->where(\DB::raw('employee.level_id'),$request->levels);
        }
        if(!empty($request->groups)){
            $model->where(\DB::raw('employee.group_id'),$request->groups);
        }
        if(!empty($request->departments)){
            $model->where(\DB::raw('employee.department_id'),$request->departments);
        }
        if(!empty($request->employee_status_id)){
            $model->where(\DB::raw('employee.employee_status_id'),$request->employee_status_id);
        }
        if(!empty($request->hospital_id)){
            $model->where(\DB::raw('employee.hospital_id'),$request->hospital_id);
        }
        if(!empty($request->employee_level_id)){
            $model->where(\DB::raw('employee.employee_level_id'),$request->employee_level_id);
        }
        $model->select([
            'employee.*',
            'employee.id as employeeid',
            'employee_status.id as employee_statusid',
            'employee_status.name as esname',
            'branch.branch_name as branch_name',
            'branch.id as branchid',
            'department.id as departmentid',
            'department.name as dname',
            'level.id as levelid',
            'level.name as lname',
            'company.id as companyid',
            'company.name as cname',
            'groups.id as groupsid',
            'groups.name as gname',
            'education_level.id as educationlevelid',
            'education_level.name as educationname',
            'hospitals.name as hname',
            'employee_level.name as ename',
            // age calculate year
            \DB::raw("DATEDIFF(year, employee.birthday, GETDATE()) as age_year"),
            // age calculate month
            \DB::raw("DATEDIFF(month, employee.birthday, GETDATE()) as age_month"),
            // workage calculate
            \DB::raw("DATEDIFF(year, employee.startworking_date, GETDATE()) as workage"),
            // workage calculate
            \DB::raw("DATEDIFF(month, employee.startworking_date, GETDATE()) as workage_month")
        ]);
        return $model;
    }

    public function employee_experience_query(Request $request){
        $model = Employeeexperience::query();
        $model->leftjoin('employee','employee.id','employee_experience.employee_id');
        $model->leftjoin('education_level','employee_experience.education_level','education_level.id');
        $model->leftjoin('branch','employee.branch_id','branch.id');
        $model->leftjoin('company','employee.company_id','company.id');
        $model->leftjoin('groups','employee.group_id','groups.id');
        $model->leftjoin('department','employee.department_id','department.id');
        $model->leftjoin('level','employee.level_id','level.id');
        $model->leftjoin('employee_status','employee.employee_status_id','employee_status.id');
        $model->leftjoin('employee_level','employee.employee_level_id','employee_level.id');
        $model->leftjoin('hospitals','employee.hospital_id','hospitals.id');
        $model->orderBy('employee.empcode','ASC');
        if(!empty($request->employee_id)){
            $model->where(\DB::raw('employee.id'),$request->employee_id);
        }
        if(!empty($request->companies)){
            $model->where(\DB::raw('employee.company_id'),$request->companies);
        }
        if(!empty($request->branches)){
            $model->where(\DB::raw('employee.branch_id'),$request->branches);
        }
        if(!empty($request->levels)){
            $model->where(\DB::raw('employee.level_id'),$request->levels);
        }
        if(!empty($request->groups)){
            $model->where(\DB::raw('employee.group_id'),$request->groups);
        }
        if(!empty($request->departments)){
            $model->where(\DB::raw('employee.department_id'),$request->departments);
        }
        if(!empty($request->employee_status_id)){
            $model->where(\DB::raw('employee.employee_status_id'),$request->employee_status_id);
        }
        if(!empty($request->hospital_id)){
            $model->where(\DB::raw('employee.hospital_id'),$request->hospital_id);
        }
        if(!empty($request->employee_level_id)){
            $model->where(\DB::raw('employee.employee_level_id'),$request->employee_level_id);
        }
        $model->select([
            'employee.empcode'
            ,'employee.prename'
            ,'employee.firstname'
            ,'employee.lastname'
            ,'employee.nickname'
            ,'employee_experience.place'
            ,'education_level.name as level_subject'
            ,'employee_experience.level as level_name'
            ,'employee_experience.subject'
            ,'employee_experience.start_date'
            ,'employee_experience.end_date'
        ]);
        return $model;
    }

    public function employee_longevity(Request $request){
        $data['title'] = "ข้อมูลอายุงาน";
        $data['request'] = $request->all();
        try {
            ini_set('memory_limit','2048M');
            $data['data'] = $this->employee_query($request)->get();
            $pdf = PDF::loadView('pdf.employee_longevity_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
            return @$pdf->stream();
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }

    public function employee_age(Request $request){
        $data['title'] = "อายุพนักงาน";
        $data['request'] = $request->all();
        try {
            ini_set('memory_limit','2048M');
            $data['data'] = $this->employee_query($request)->get();
            $pdf = PDF::loadView('pdf.employee_age_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
            return @$pdf->stream();
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }

    public function employee_resign(Request $request){
        $data['title'] = "ข้อมูลรายชื่อพนักงานการลาออก";
        $data['request'] = $request->all();
        try {
            ini_set('memory_limit','2048M');
            $data['data'] = $this->employee_query($request)->where('employee.employee_status_id','2')->get();
            $pdf = PDF::loadView('pdf.employee_resign_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
            return @$pdf->stream();
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }

    public function employee_current(Request $request){
        $data['title'] = "ข้อมูลรายชื่อพนักงานปัจจุบัน";
        $data['request'] = $request->all();
        try{
            ini_set('memory_limit','2048M');
            $result = $this->employee_query($request)->where('employee.employee_status_id','3')->get();
            foreach ($result as $key => $value) {
                $value->through_trial = date('Y-m-d',strtotime("$value->startworking_date + 3 month"));
                $return[] = $value;
            }
            $data['data'] = $return;
            $pdf = PDF::loadView('pdf.employee_current_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
            return @$pdf->stream();
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }

    public function employee_tain(Request $request){
        $data['title'] = "ข้อมูลรายชื่อพนักงานทดลองงาน";
        $data['request'] = $request->all();
        try{
            ini_set('memory_limit','2048M');
            $result = $this->employee_query($request)->where('employee.employee_status_id','1')->get();
            foreach ($result as $key => $value) {
                $value->through_trial = date('Y-m-d',strtotime("$value->startworking_date + 3 month"));
                $return[] = $value;
            }
            $data['data'] = $return;
            $pdf = PDF::loadView('pdf.employee_tain_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
            return @$pdf->stream();
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }

    public function employee_hospital(Request $request){
        $data['title'] = "ข้อมูลประกันสังคม";
        $data['request'] = $request->all();
        try {
            ini_set('memory_limit','2048M');
            $data['data'] = $this->employee_query($request)->get();
            $pdf = PDF::loadView('pdf.employee_hospital_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
            return @$pdf->stream();
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }

    public function employee_education(Request $request){
        $data['title'] = "ข้อมูลการศึกษา";
        $data['request'] = $request->all();
        try {
            ini_set('memory_limit','2048M');
            $data['data'] = $this->employee_experience_query($request)->where([
                'employee_experience.experience_type' =>'e'
            ])->get();
            $pdf = PDF::loadView('pdf.employee_education_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
            return @$pdf->stream();
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }

    public function employee_contact(Request $request){
        $data['title'] = "ข้อมูลติดต่อ E-mail เบอร์โทร";
        $data['request'] = $request->all();
        try {
            ini_set('memory_limit','2048M');
            $data['data'] = $this->employee_query($request)->get();
            $pdf = PDF::loadView('pdf.employee_contact_list',$data)->setPaper('a4', 'landscape')->setOptions(["enable_php", true]);
            return @$pdf->stream();
            // return $pdf->download($data['title'].'.pdf');
        }catch(\Throwable $th){
            return $th->getMessage();
        }
    }
    
    
}