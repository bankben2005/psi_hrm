<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;

class AdminController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function index()
    {
        $count_gender = \DB::select('SELECT e.gender,COUNT (*) AS "c" FROM employee e WHERE e.resignation_date IS NULL GROUP BY e.gender;');
        $data['count_gender'] = (!empty($count_gender))?$count_gender:[];

        $count_company = \DB::select('SELECT e.company_id,c.name,COUNT (*) AS "c" FROM employee e JOIN company c ON c.id=e.company_id WHERE e.resignation_date IS NULL GROUP BY e.company_id,c.name; ');
        $data['count_company'] = (!empty($count_company))?$count_company:[];

        $count_group = \DB::select('SELECT e.group_id,(SELECT g.name FROM groups g WHERE g.id =e.group_id) AS g,COUNT (*) AS "c" FROM employee e WHERE e.resignation_date IS NULL GROUP BY e.group_id ORDER BY c DESC;');
        $data['count_group'] = (!empty($count_group))?$count_group:[];

        $count_department = \DB::select('SELECT e.department_id,(SELECT d.name FROM department d WHERE d.id =e.department_id) AS d,COUNT (*) AS c FROM employee e WHERE e.resignation_date IS NULL GROUP BY e.department_id ORDER BY c DESC;');
        $data['count_department'] = (!empty($count_department))?$count_department:[];

        $count_level = \DB::select('SELECT e.level_id,(SELECT l.name FROM LEVEL l WHERE l.id =e.level_id) AS l,COUNT (*) AS c FROM employee e WHERE e.resignation_date IS NULL GROUP BY e.level_id ORDER BY c DESC;');
        $data['count_level'] = (!empty($count_level))?$count_level:[];

        $count_employee_level = \DB::select('SELECT e.employee_level_id,(SELECT el.name FROM employee_level el WHERE el.id =e.employee_level_id) AS el,COUNT (*) AS c FROM employee e WHERE e.resignation_date IS NULL GROUP BY e.employee_level_id;');
        $data['count_employee_level'] = (!empty($count_employee_level))?$count_employee_level:[];

        $count_range = \DB::select("
            SELECT
                t.range as [range], count(*) as [c]
            FROM (
                select case  
                    when DATEDIFF(MONTH, e.startworking_date , GETDATE()) between 0 and 3 then '1 (0-3ด)'
                when DATEDIFF(MONTH, e.startworking_date , GETDATE()) between 4 and 12 then '2 (4ด-1ป)'
                when DATEDIFF(MONTH, e.startworking_date , GETDATE()) between 13 and 60 then '3 (1-5ป)'
                    when DATEDIFF(MONTH, e.startworking_date , GETDATE()) between 61 and 120 then '4 (5-10ป)'
                    when DATEDIFF(MONTH, e.startworking_date , GETDATE()) between 121 and 240 then '5 (0-20ป)'
                    when DATEDIFF(MONTH, e.startworking_date , GETDATE()) >=241 then '6 (20ขั้นไป)'
                    else '7 (อื่นๆ)'
                end as range
            from employee e
                where e.startworking_date IS NOT NULL
                and e.resignation_date IS NULL
            ) t
            GROUP BY
                t.range;
        ");

        $data['count_range'] = (!empty($count_range))?$count_range:[];

        $count_age = \DB::select("
            SELECT
                t.range as [range], count(*) as [c]
            FROM (
                select case  
                    when DATEDIFF(MONTH, e.birthday , GETDATE()) between 0 and 3 then '20-25y'
                when DATEDIFF(MONTH, e.birthday , GETDATE()) between 4 and 12 then '25-35y'
                when DATEDIFF(MONTH, e.birthday , GETDATE()) between 13 and 60 then '35-45y'
                    when DATEDIFF(MONTH, e.birthday , GETDATE()) between 61 and 120 then '45-55y'
                    when DATEDIFF(MONTH, e.birthday , GETDATE()) between 121 and 240 then '55+'
                    else 'other'
                end as range
            from employee e
                where e.birthday IS NOT NULL
                and e.startworking_date IS NOT NULL
                and e.resignation_date IS NULL
            ) t
            GROUP BY
                t.range;
        ");

        $data['count_age'] = (!empty($count_age))?$count_age:[];

        // return response()->json(($data));
        return view('admin.chart',$data); // admin/chart
    }
    
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::guard('admin')->attempt($credentials)) {
            // Authentication passed...
            return \Redirect::to("/admin");
        }else{
            return \Redirect::to("/admin/login");
        }
    }

    public function login(){
        if(\AUTH::guard('admin')->check()){
            return redirect('admin');
        }else{
            return view('auth.admin_login'); // auth/admin_login
        }
        
    }

    public function logout(){
        \Session::flush();
        \Auth::guard("admin")->logout();
        return \Redirect::to("/admin/login");
    }

}