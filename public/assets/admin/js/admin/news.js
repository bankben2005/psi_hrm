var News = (function () {
  var handleTables = function () {
    var table = $('#news').DataTable({
      "responsive": true,
      "serverSide": true,
      "processing": true,
      "ajax": rurl + 'admin/news/list',
      "language": {
        "url": rurl + "assets/plugins/datatable_th.json"
      },
      "columns": [{
          "data": 'DT_RowIndex',
          "name": 'DT_RowIndex',
          orderable: false,
          searchable: false,
          className: "text-center"
        },
        {
          "data": "picture",
          "name": "news.picture"
        },
        {
          "data": "name",
          "name": "news.name"
        },
        {
          "data": "employeename",
          "name": "employee.firstname"
        },
        {
          "data": "lastname",
          "name": "employee.lastname",
          "visible": false
        },
        {
          "data": "newscreatedat",
          "name": "news.created_at"
        },
        {
          "data": "action",
          orderable: false,
          searchable: false
        }
      ],
    });
  }

  var handleValidation = function () {
    var form = $('.validateForm');
    var btn = $('.validateForm [type="submit"]');

    form.validate({
      errorElement: 'span', // default input error message container
      errorClass: 'help-block help-block-error', // default input error message class
      focusInvalid: false, // do not focus the last invalid input
      ignore: '', // validate all fields including form hidden input

      rules: {
        name: {
          required: true
        },
        detail: {
          required: true
        },
        created_by: {
          required: true
        },
      },
      highlight: function (element) { // hightlight error inputs
        $(element)
          .closest('.form-group .form-control').addClass('is-invalid') // set invalid class to the control group
      },
      unhighlight: function (element) { // revert the change done by hightlight
        $(element)
          .closest('.form-group .form-control').removeClass('is-invalid') // set invalid class to the control group
          .closest('.form-group .form-control').addClass('is-valid')
      },
      errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
          error.insertAfter(element.parent())
        } else {
          error.insertAfter(element)
        }
      },
      success: function (label) {
        label
          .closest('.form-group .form-control').removeClass('is-invalid') // set success class to the control group
      },
      submitHandler: function (element) {
        btn.prop('disabled', true)
        $.ajax({
          type: "post",
          url: rurl + 'admin/news',
          data: $(element).serialize(),
          dataType: "html",
          success: function (data) {
            btn.prop('disabled', false)
            $('[data-dismiss="modal"]').trigger('click');
            $('.validateForm').removeClass('formadd')
            $('.validateForm').removeClass('formedit')
            $("#news").DataTable().ajax.reload(null, false);
            swal('ยินดีด้วย!', data, "success")
          },
          error: function (data) {
            $('.validateForm').removeClass('formadd')
            $('.validateForm').removeClass('formedit')
            
          }
        });
      }
    })
  }

  var handleButton = function () {
    $(document).on('click', '.btn-delete', function () {
      $this = $(this)
      swal({
          title: "คุณแน่ใจไหม?",
          text: "เมื่อลบแล้วคุณจะไม่สามารถกู้คืนไฟล์นี้ได้!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "ใช่, ยืนยัน!",
          cancelButtonText: "ยกเลิก",
          closeOnConfirm: false
        },
        function () {
          deleteUser($this);
        });
    })

    $(document).on('click', '.btn-add', function () {
      $('[name="id"]').val(null)
      $('[name="detail"]').summernote({'reset':true,height: "300px"});
      $('img#holder').removeAttr('src');
      //remove form class
      $('.validateForm').removeClass('formadd')
      $('.validateForm').removeClass('formedit')

      $('.validateForm').trigger('reset')
      $('.validateForm').addClass('formadd')
      $('.validateForm').removeAttr('data-id')

      //add value to form example
      $('.ls-select2').select2();
    })

    $(document).on('click', '.btn-edit', function (btn) {
      //remove form class
      $('.validateForm').removeClass('formadd');
      $('.validateForm').removeClass('formedit');

      var id = $(this).data('id');
      $('[name="id"]').val(id);

      var selector = $('.validateForm');
      selector.addClass('formedit');
      selector.find('[type="submit"]').removeAttr('data-id');
      selector.find('[type="submit"]').attr('data-id', id);
      $.ajax({
        type: 'get',
        url: rurl + 'admin/news/' + id,
        dataType: "json",
        success: function (data) {
          // console.log(data);
          Object.entries(data).forEach(entry => {
            $('[name="' + entry[0] + '"]').val(entry[1]);
          });
          $('#holder').attr('src', surl + data.picture);
          $('#summernote-editor').summernote({'reset':true,height: "300px"});
          $('#summernote-editor').summernote('code', (data.detail));
          $('.ls-select2').select2();
          $('#modalSlideUp').modal('show');
        },
        error: function (data) {
          swal("ไม่พบข้อมูลที่ท่านต้องการ!", data.responseTextta, "error");
        }
      })
    })

    $(document).on('click', '.formadd [type="submit"]', function () {
      handleValidation();
    })

    $(document).on('click', '.formedit [type="submit"]', function (e) {
      handleValidation();
    })
  }

  var deleteUser = function (value) {
    var id = value.data('id');
    var token = value.data('token');

    $.ajax({
      url: rurl + 'admin/news/' + id,
      type: 'DELETE',
      data: {
        _method: 'delete',
        _token: token,
        _id: id
      },
      success: function (data) {
        $("#news").DataTable().ajax.reload(null, false);
        swal('ยินดีด้วย!', data, "success");
      },
      error: function (data) {
        swal("พบข้อผิดผลาด!", data, "error");
      }
    })
  }

  var newsForm = function () {
    $(document).ready(function () {
      $('#lfm').filemanager('image');
      // Define function to open filemanager window
      var lfm = function (options, cb) {
        var route_prefix = (options && options.prefix) ? options.prefix : '/admin/laravel-filemanager';
        window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager',
          'width=900,height=600');
        window.SetUrl = cb;
      }

      // Define LFM summernote button
      var LFMButton = function (context) {
        var ui = $.summernote.ui;
        var button = ui.button({
          contents: '<i class="note-icon-picture"></i> ',
          tooltip: 'Insert image with filemanager',
          click: function () {
            lfm({
              type: 'image',
              prefix: '/laravel-filemanager'
            }, function (lfmItems, path) {
              console.log(lfmItems);
              $('#summernote-editor').summernote('editor.insertImage', lfmItems);
            });

          }
        });
        return button.render();
      };

      var smn = $('#summernote-editor').summernote({
        dialogsInBody: true,
        height: "300px",
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'italic', 'underline', 'clear']],
          ['fontname', ['fontname', 'fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['table', ['table']],
          ['insert', ['link', 'picture', 'video', 'hr']],
          ['view', ['fullscreen', 'codeview']],
          ['help', ['help']],
          ['popovers', ['lfm']]
        ],
        buttons: {
          lfm: LFMButton
        },
      })
    });
  }

  return {
    // main function to initiate the module
    init: function () {
      handleTables();
      handleButton();
      newsForm();
    }
  }
})()

jQuery(document).ready(function () {
  News.init();
})