$(document).on('change', '#provinces', function(){
    $('select#districts').empty();
    $('select#districts').append('<option value="">== อำเภอ/เขต ==</option>');
    $('select#subdistricts').empty();
    $('select#subdistricts').append('<option value="">== ตำบล/แขวง ==</option>');
    $('#zipcode').val(null);
    $.ajax({
        type: "get",
        url: rurl + 'getdistrict/' + this.value,
        dataType: "json",
        success: function (response) {
            $.each(response, function (index, value) {
                $('select#districts').append('<option value="' + value.id + '">' +
                    value.name_th + '</option>');
            });
            $('.ls-select2').select2();
        }
    });
});

$(document).on('change', '#districts', function(){
    $('select#subdistricts').empty();
    $('select#subdistricts').append('<option value="">== ตำบล/แขวง ==</option>');
    $('#zipcode').val(null);
    $.ajax({
        type: "get",
        url: rurl + 'getsubdistrict/' + this.value,
        dataType: "json",
        success: function (response) {

            $.each(response, function (index, value) {
                $('select#subdistricts').append('<option value="' + value.id + '">' +
                    value.name_th + '</option>');
            });
            $('.ls-select2').select2();
        }
    });
});

$(document).on('change', '#subdistricts', function(){
    $.ajax({
        type: "get",
        url: rurl + 'getzipcode/' + this.value,
        dataType: "json",
        success: function (response) {
            console.log(response);
            $('#zipcode').val(response);
        }
    });
});

function redrawaddress(subdistrict_id){
    $.ajax({
        type: "get",
        url: rurl + 'getaddress/' + subdistrict_id,
        dataType: "json",
        success: function (response) {
            $('select#provinces').empty();
            $('select#provinces').append('<option value="">== จังหวัด ==</option>');
            $.each(response.provinces, function (index, value) {
                if(response.province_id==value.id){
                    $('select#provinces').append('<option value="' + value.id + '" selected>' +
                    value.name_th + '</option>');
                }else{
                    $('select#provinces').append('<option value="' + value.id + '">' +
                    value.name_th + '</option>');
                }
            });
            $('select#districts').empty();
            $('select#districts').append('<option value="">== อำเภอ/เขต ==</option>');
            $.each(response.districts, function (index, value) {
                if(response.district_id==value.id){
                    $('select#districts').append('<option value="' + value.id + '" selected>' +
                    value.name_th + '</option>');
                }else{
                    $('select#districts').append('<option value="' + value.id + '">' +
                    value.name_th + '</option>');
                }
            });
            $('select#subdistricts').empty();
            $('select#subdistricts').append('<option value=""> == ตำบล/แขวง == </option>');
            $.each(response.subdistricts, function (index, value) {
                if(response.subdistrict_id==value.id){
                    $('select#subdistricts').append('<option value="' + value.id + '" selected>' +
                    value.name_th + '</option>');
                }else{
                    $('select#subdistricts').append('<option value="' + value.id + '">' +
                    value.name_th + '</option>');
                }
            });
            $('.ls-select2').select2();
        }
    });
}