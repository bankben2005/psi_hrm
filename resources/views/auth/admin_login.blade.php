<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>PSI - HRM ระบบพัฒนาบุคลากร</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="{{asset('pages/ico/60.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('pages/ico/76.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('pages/ico/120.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('pages/ico/152.png')}}">
    <link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="{{asset('assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="{{asset('pages/css/themes/light.css')}}" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="{{asset('css/admin/custom.css')}}" rel="stylesheet" type="text/css" />

</head>

<body class="fixed-header menu-pin">

    <div class="register-container full-height sm-p-t-30">
        <div class="d-flex justify-content-center flex-column full-height ">

            @php
                if($result = \App\Http\Controllers\Admin\SettingController::show('login_picture')){
                    $img = $result;
                }else{
                    $img = '';
                }
            @endphp

            <img src="{{url($img)}}" alt="logo" data-src="{{url($img)}}" data-src-retina="{{url($img)}}" width="550px" height="auto">

            <!-- START Login Form -->
            <form id="form-login" class="p-t-15" role="form" method="POST" action="{{ url('/admin/login') }}">
                @csrf
                <!-- START Form Control-->
                <div class="form-group form-group-default">

                    <label for="email">{{ __('ชื่อผู้ใช้') }}</label>
                    <div class="controls">
                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <!-- END Form Control-->
                <!-- START Form Control-->
                <div class="form-group form-group-default">
                    <label for="password">{{ __('รหัสผ่าน') }}</label>
                    <div class="controls">
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password" required
                            autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="float-right">
                    <button class="btn btn-primary btn-cons m-t-10" type="submit">เข้าสู่ระบบ</button>
                </div>
            </form>
            <!--END Login Form-->
        </div>
    </div>

    <!-- BEGIN VENDOR JS -->
    <script src="{{asset('assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/modernizr.custom.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/popper/umd/popper.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript">
    </script>
    <!-- END VENDOR JS -->
    <script src="{{asset('pages/js/pages.min.js')}}"></script>
    <script>
        $(function () {
            $('#form-login').validate()
        })
    </script>
</body>

</html>