@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <form method="post" action="{{url("/admin/employee/create")}}" class="form">
                        @csrf
                        <input name="id" value="1">
                        <a class="btn btn-xs btn-warning btn-edit">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn-delete btn btn-xs" href="#" data-id="1">
                            <i class="fa fa-trash"></i>
                        </a>
                        <button type="summit">submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <form class="hawkeye">
                        <input type="text" name="0" id="">
                        <input type="text" name="1" id="">
                        <input type="text" name="2" id="">
                        <input type="text" name="3" id="">
                        <input type="text" name="4" id="">
                        <input type="text" name="5" id="">
                        <input type="text" name="6" id="">
                        <input type="text" name="7" id="">
                        <input type="text" name="8" id="">
                        <input type="text" name="9" id="">
                        <input type="text" name="10" id="">
                        <input type="text" name="11" id="">
                        <input type="text" name="12" id="">
                        <input type="text" name="13" id="">
                        <input type="text" name="14" id="">
                        <input type="text" name="15" id="">
                        <input type="text" name="16" id="">
                        <input type="text" name="17" id="">
                        <input type="submit" name="18" id="">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>
    $(document).ready(function () {
        $('form.hawkeye').submit(function (e) {
            e.preventDefault();
            console.log($('form').serialize());
        });
    });
</script>
</body>
</html> -->