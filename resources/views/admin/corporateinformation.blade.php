@extends('admin.layouts.app')

@section('style')
    <link href="{{asset('assets/plugins/summernote/summernote-bs4.css')}}" rel="stylesheet" type="text/css" media="screen">
@stop

@section('script')
    <script src="{{asset('assets/plugins/summernote/summernote-bs4.js')}}" type="text/javascript"></script>
    <script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script src="{{asset('assets/admin/js/admin/corporateinformation.js')}}"></script>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
        <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
            + {{ isset($menu) ? $menu : '' }}
        </button>
    </div>
    <div class="card-body">
        <table id="corporateinformation" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ภาพข่าว</th>
                    <th>พาดหัวข่าว</th>
                    <th>โดย</th>
                    <th>นามสกุล</th>
                    <th>โพสต์เมื่อ</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<form class="validateForm">
    <div class="modal fade slide-up" id="modalSlideUp" role="dialog" aria-hidden="false">
        
        <div class="modal-dialog modal-full" style="width:100%;">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">
                            <i class="pg-close"></i>
                        </button>
                        <h5>{{ isset($menu) ? $menu : '' }}</h5>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label text-right">พาดหัวข่าว</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" placeholder="พาดหัวข่าว" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="picture" class="col-sm-2 col-form-label text-right">ภาพข่าว</label>
                            <div class="col-sm-10" style="padding:5px;background:rgba(100,100,100,0.1);">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-default">
                                        <i class="fa fa-picture-o"></i> เลือก
                                        </a>
                                    </span>
                                    <input id="thumbnail" class="form-control" type="text" name="picture" placeholder="ภาพข่าว">
                                </div>
                                <img id="holder" class="img-fluid" style="margin-top:5px;max-height:150px;">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="detail" class="col-sm-2 col-form-label text-right">เนื้อหา</label>
                            <div class="col-sm-10">
                                <textarea name="detail" id="summernote-editor" class="form-control input-sm"
                                    placeholder="เนื้อหา"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="created_by" class="col-sm-2 col-form-label text-right">โดย</label>
                            <div class="col-sm-10">
                            <input type="hidden" name="created_by" placeholder="โดย" value="{{\Auth::guard('admin')->user()->id}}" class="form-control input-sm">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@stop