@extends('admin.layouts.app')

@section('script')
<script src="{{asset('assets/admin/js/admin/branch.js')}}"></script>
<script src="{{asset('assets/admin/js/admin/custom.js')}}"></script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
		<button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
			+ {{ isset($menu) ? $menu : '' }}
		</button>
	</div>
	<div class="card-body">
		<table id="branch" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>สาขา</th>
					<th>เลขที่</th>
					<th>ตำบล/แขวง</th>
					<th>อำเภอ/เขต</th>
					<th>จังหวัด</th>
					<th>ไปรษณีย์</th>
					<th>เริ่มงาน</th>
					<th>เลิกงาน</th>
					<th>ระยะลงเวลา</th>
					<th>สถานะ</th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<form class="validateForm">
	<div class="modal fade slide-up" id="modalSlideUp" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
								class="pg-close fs-14"></i>
						</button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="branch_name" class="col-sm-2 col-form-label">สาขา</label>
							<div class="col-sm-10">
								<input type="text" name="branch_name" placeholder="สาขา" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="latitude" class="col-sm-2 col-form-label">ละติจูด</label>
							<div class="col-sm-10">
								<input type="text" name="latitude" placeholder="ละติจูด" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="longitude" class="col-sm-2 col-form-label">ลองจิจูด</label>
							<div class="col-sm-10">
								<input type="text" name="longitude" placeholder="ลองจิจูด"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="address" class="col-sm-2 col-form-label">เลขที่</label>
							<div class="col-sm-10">
								<input type="text" name="address" placeholder="เลขที่" class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="name_th" class="col-sm-2 col-form-label">จังหวัด</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="province_id" id="provinces">
									<option value="">== จังหวัด ==</option>
									@foreach ($provinces as $key => $item)
									<option value="{{$item->id}}">{{$item->name_th}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="name_th" class="col-sm-2 col-form-label">อำเภอ/เขต</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="district_id" id="districts">
									<option value="">== อำเภอ/เขต ==</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="name_th" class="col-sm-2 col-form-label">ตำบล/แขวง</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="subdistrict_id" id="subdistricts">
									<option value="">== ตำบล/แขวง ==</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="working_time_start" class="col-sm-2 col-form-label">ไปรษณีย์</label>
							<div class="col-sm-10">
								<input type="text" name="zipcode" placeholder="ไปรษณีย์" class="form-control input-sm" id="zipcode">
							</div>
						</div>
						<div class="form-group row">
							<label for="working_time_start" class="col-sm-2 col-form-label">เริ่มงาน</label>
							<div class="col-sm-10">
								<input type="time" name="working_time_start" placeholder="เริ่มงาน"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="working_time_end" class="col-sm-2 col-form-label">เลิกงาน</label>
							<div class="col-sm-10">
								<input type="time" name="working_time_end" placeholder="เลิกงาน"
									class="form-control input-sm">
							</div>
						</div>
						<div class="form-group row">
							<label for="distance" class="col-sm-2 col-form-label">ระยะลงเวลา</label>
							<div class="col-sm-10">
								<input type="text" name="distance" placeholder="100 ม."
									class="form-control input-sm">
							</div>
						</div>

						<div class="form-group row">
							<label for="status" class="col-sm-2 col-form-label">การใช้งาน</label>
							<div class="col-sm-10">
								<select class="ls-select2" name="status">
									<option value="">== สถานะ ==</option>
									<option value="T">เปิด</option>
									<option value="F">ปิด</option>
								</select>
							</div>
						</div>
						

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@stop