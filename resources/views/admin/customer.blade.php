@extends('admin.layouts.app')

@section('script')
<script src="{{asset('assets/admin/js/admin/customer.js')}}"></script>
@stop

@section('content')
<div class="card">
	<div class="card-header">
		<h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
		<button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalFillIn">
			+ {{ isset($menu) ? $menu : '' }}
		</button>
	</div>
	<div class="card-body">
		<table id="customer" class="table table-xs table-hover table-bordered table-responsive" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>ชื่อ</th>
					<th>นามสกุล</th>
					<th>อีเมล</th>
					<th>เบอร์มือถือ</th>
					<th>เบอร์โทรศัพท์</th>
					<th>APP ID</th>
					<th>AUTHORIZATION</th>
					<th>อนุมัติโดย</th>
					<th>อนุมัติวันที่</th>
					<th>ถึงวันที่</th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<form class="validateForm">
	<div class="modal fade slide-up disable-scroll" id="modalFillIn" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-lg" style="width:100%;">
			<div class="modal-content-wrapper">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
							class="pg-close fs-14"></i>
                        </button>
						<h5>{{ isset($menu) ? $menu : '' }}</h5>
					</div>
					<div class="modal-body">
						<input class="form-control" type="hidden" name="id">
						<div class="form-group row">
							<label for="firstname" class="col-sm-3 col-form-label">ชื่อ</label>
							<div class="col-sm-9">
								<input type="text" name="firstname" placeholder="ชื่อ"
									class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="lastname" class="col-sm-3 col-form-label">นามสกุล</label>
							<div class="col-sm-9">
								<input type="text" name="lastname" placeholder="นามสกุล" class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="email" class="col-sm-3 col-form-label">อีเมล</label>
							<div class="col-sm-9">
								<input type="email" name="email" placeholder="อีเมล" class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="mobile" class="col-sm-3 col-form-label">เบอร์มือถือ</label>
							<div class="col-sm-9">
								<input type="text" name="mobile" placeholder="เบอร์มือถือ" class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="telephone" class="col-sm-3 col-form-label">เบอร์โทรศัพท์</label>
							<div class="col-sm-9">
								<input type="text" name="telephone" placeholder="เบอร์โทรศัพท์"
									class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="app_id" class="col-sm-3 col-form-label">APP ID</label>
							<div class="col-sm-9">
								<input type="text" name="app_id" placeholder="APP ID" class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="authorization" class="col-sm-3 col-form-label">authorization</label>
							<div class="col-sm-9">
								<input type="text" name="authorization" placeholder="authorization"
									class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="firstname" class="col-sm-3 col-form-label">อนุมัติโดย</label>
							<div class="col-sm-9">
								<select class="ls-select2" name="approve_by">
									<option value="">== อนุมัติโดย ==</option>
									@foreach ($employee as $key => $item)
									<option value="{{$item->id}}">{{$item->firstname}} {{$item->lastname}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="approve_date" class="col-sm-3 col-form-label">อนุมัติวันที่</label>
							<div class="col-sm-9">
								<input type="date" name="approve_date" placeholder="approve_date"
									class="form-control">
							</div>
						</div>
						<div class="form-group row">
							<label for="end_date" class="col-sm-3 col-form-label">ถึงวันที่</label>
							<div class="col-sm-9">
								<input type="date" name="end_date" placeholder="end_date" class="form-control">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btn-cons">บันทึก</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@stop