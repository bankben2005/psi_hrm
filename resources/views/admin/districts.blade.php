@extends('admin.layouts.app')

@section('script')
<script src="{{asset('assets/admin/js/admin/districts.js')}}"></script>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <h5 class="pull-left">{{ isset($menu) ? $menu : '' }}</h5>
        <button type="button" class="btn btn-theme btn-add pull-right" data-toggle="modal" data-target="#modalSlideUp">
            + {{ isset($menu) ? $menu : '' }}
        </button>
    </div>
    <div class="card-body">
        <table id="districts" class="table table-xs table-hover table-bordered table-striped dataTable no-footer" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>จังหวัด</th>
                    <th>รหัสอำเภอ/เขต</th>
                    <th>ชื่อไทย</th>
                    <th>ชื่ออังกฤษ</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<form class="validateForm">
    <div class="modal fade slide-up disable-scroll" id="modalSlideUp" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                        </button>
                        <h5>{{ isset($menu) ? $menu : '' }}</h5>
                        {{-- <p class="p-b-10"></p> --}}
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id">
                        <div class="form-group row">
                            <label for="name_th" class="col-sm-2 col-form-label">จังหวัด</label>
                            <div class="col-sm-10">
                                <select class="ls-select2" name="province_id">
                                    <option value="">== จังหวัด ==</option>
                                    @foreach ($provinces as $key => $item)
                                    <option value="{{$item->id}}">{{$item->name_th}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="code" class="col-sm-2 col-form-label">รหัสอำเภอ/เขต</label>
                            <div class="col-sm-10">
                                <input type="text" name="code" placeholder="code" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name_th" class="col-sm-2 col-form-label">ชื่อไทย</label>
                            <div class="col-sm-10">
                                <input type="text" name="name_th" placeholder="name_th" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name_en" class="col-sm-2 col-form-label">ชื่ออังกฤษ</label>
                            <div class="col-sm-10">
                                <input type="text" name="name_en" placeholder="name_en" class="form-control input-sm">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-cons" data-dismiss="modal">ยกเลิก</button>
                        <button type="submit" class="btn btn-success btn-cons">บันทึก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@stop